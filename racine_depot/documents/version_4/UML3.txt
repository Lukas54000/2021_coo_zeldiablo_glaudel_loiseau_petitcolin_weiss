@startuml

interface Objet {
    +executer(Personnage) : void
    +getTypeObjet() : String
}
interface Effet {
    +declancherEffet(Personnage, Monstre) : void
}
class Cle {
    +Cle()
    +executer(Personnage) : void
    +getTypeObjet() : String
}
class Potion {
    +Potion()
    +executer(Personnage) : void
    +getTypeObjet() : String
}
class Piege {
    +Piege()
    +declancherEffet(Personnage, Monstre) : void
}
class CaseLibre {
    +CaseLibre(int, int, Objet, Effet)
    +retirerObjet() : void
    +avoirObjet() : boolean
    +getObjet() : Objet
    +getEffet() : Effet
}
class CaseDepart {
    +CaseDepart(int, int)
}
class CaseSortie {
    +CaseSortie(int, int)
}
class CaseTrou {
    +CaseTrou(int, int)
    +remplirTrou() : void
}
class CaseGlace {
    +CaseGlace(int, int)
}
class CaseMur {
    +CaseMur(int, int)
}
abstract Monstre {
    -estTraversable : boolean 
    -pointDeVie : int
    -puissance : int
    -typeMonstre : int
    {static} +MONSTRE_IMMOBILE : int
    {static} +MONSTRE_ALEATOIRE : int
    {static} +MONSTRE_INTELLIGENT : int
    {static} +MONSTRE_TRES_INTELLIGENT : int
    +Monstre(Case, int, int)
    +modifierPv(int) : void
    +etreMort() : boolean 
    +persoAdjacent(Personnage) : boolean 
    +attaquer(Personnage) : void 
    {abstract} + seDeplacer(Donjon) : void 
    +getCase() : Case 
    +setCase(Case) : Case 
    +getVie() : int 
    +setVie(int) : void
    +getPuissance() : int 
    +etreTraversable() : boolean 
    +getTypeMonstre(): int 
    +setTypeMonstre(int) : void

}
class Personnage {
    -pv : int 
    -avoirCle : boolean 
    -direction : String 
    -force : int 
    -modeGlissant : boolean
    {static} +PV_PERSO : int
    +Personnage(CaseLibre) 
    +seDeplacer(Donjon, Case) : void 
    +etreVivant() : boolean 
    +modifierPv(int) : void 
    +attaquer(Donjon) : void
    +changerCle() : void 
    +setCase(CaseLibre) : void
    +setDirection(String) : void
    +getPv() : int
    +getAvoirCle() : boolean
    +getForce() : int 
    +getDirection() : String 
    +getModeGlissade() : boolean
    +getCase() : CaseLibre
    +getCaseDirection(Donjon, Case) : Case
}
class Pierre {
    +Pierre(Case) 
    +changerCase(Donjon, Case) : boolean 
    +getCase() : Case
}
class Niveau {
    +Niveau(String)
    +getMonstreInit() : ArrayList<Monstre>
    +getCase(int, int) : Case 
    +getTabCase() : Case[][]
    +getCaseImportante(int) : Case
    +getCaseObjet(String) : CaseLibre 
    +getPierre() : Pierre 
    +pierreTombe() : void
    +getTrou() : Case 
    +toString() : String 
}
class Donjon {
    -listeNiv : String[]
    -nivEnCours : int 
    -numTour : int 
    -tempEntre2Attaques : int 
    {static} +INT_BETWEEN_2_ATTAQUES : int
    {static} +INT_VITESSE_MONSTRES : int
    +Donjon() 
    +Donjon(String[])
    +verifierNouvelleCase() : void
    +chercherMonstre(Case) : Monstre
    +passerNiveauSuivant() : void 
    +caseOccupee(Case) : boolean 
    +addMonstre(Monstre) : void 
    +actionsJoueur(Commande) : void
    +actionsMonstres() : void 
    +verifierIntegriteMonstres() : void 
    +evoluer(Commande) : void 
    +etreFini() : boolean
    +getPersonnage() : Personnage 
    +getNiveau() : Niveau 
    +getNiveauEnCours() : int
    +getMonstresDuNiveau() : ArrayList<Monstre>
    +getTempsEntre2Attaques() : int 
    
}
class MonstreTresIntelligent {
    +MonstreTresIntelligent(Case)
    +seDeplacer(Donjon) : boolean
}
class MonstreImmobile {
    +MonstreImmobile(Case)
    +seDeplacer(Donjon) : boolean
}
class MonstreIntelligent {
    +MonstreIntelligent(Case) 
    +seDeplcaer(Donjon) : void
}
class MonstreAleatoire {
    +MonstreAleatoire(Case) 
    +seDeplacer(Donjon) : void
}
abstract Case {
    -x : int 
    -y : int 
    #typeDeCase : int 
    {static} +CASE_ABSTRAITE : int 
    {static} +CASE_MUR : int 
    {static} +CASE_LIBRE : int 
    {static} +CASE_DEPART : int 
    {static} +CASE_SORTIE : int 
    {static} +CASE_TROU : int 
    {static} +CASE_GLACE : int
    +Case(int, int)
    +getX() : int 
    +getY() : int 
    +getTypeDeCase() : int 
    +setX(int) : void 
    +setY(int) : void
}

Donjon --> "0..1" Personnage : -zelda
Donjon --> "0..1" Niveau : -niveau 
Donjon --> "0..*" Monstre : -monstreDuNiveau

Personnage --> "0..1" CaseLibre : -cas

Niveau --> "0..*" CaseLibre : -listCases
Niveau --> "0..1" CaseTrou : -caseT
Niveau --> "0..1" Pierre : -pierre
Niveau --> "0..*" Case : -tabCase
Niveau --> "0..*" Monstre : -listeMonstreInit

Pierre --> "0..1" Case : -position

Monstre <|-- MonstreTresIntelligent
Monstre <|-- MonstreIntelligent
Monstre <|-- MonstreImmobile
Monstre <|-- MonstreAleatoire
Monstre --> "0..1" Case : -cas

MonstreTresIntelligent <-- "0..1" Case : -direction 

Case <|-- CaseMur 
CaseLibre <|-- CaseTrou
Case <|-- CaseLibre 

CaseLibre <|-- CaseGlace 
CaseLibre <|-- CaseSortie 
CaseLibre <|-- CaseDepart
CaseLibre --> "0..1" Effet : -effet 
CaseLibre --> "0..1" Objet : -obj

Effet <|.. Piege
Objet <|.. Potion 
Objet <|.. Cle

interface Jeu {
    +evouler(Commande) : void 
    +etreFini() : boolean
}
class DessinGame {
    -pvPerso : int 
    -timeAttaque : int 
    -timeBlessure : int 
    -vieEnPlus : boolean 
    -posX : int 
    -posY : int 
    +DessinGame(Donjon)
    -dessinerObjet(BufferedImage) : void 
    +dessiner(BufferedImage) : void 
}
class Sprite {
    -listeNiveau : String[]
    +background : BufferedImage[]
    +backgroundBois : BufferedImage
    +escalierFermeHaut : BufferedImage
    +escalierFermeBas : BufferedImage
    +murPierre : BufferedImage
    +perso : BufferedImage
    +clef : BufferedImage
    +poition : BufferedImage
    +cible : BufferedImage
    +monstreImmobile : BufferedImage
    +monstreAleatoire : BufferedImage
    +monstreIntelligent : BufferedImage
    +monstreTresIntelligent : BufferedImage
    +pierre : BufferedImage
    +trou : BufferedImage
}
class Commande {
    +gauche : boolean 
    +droite : boolean 
    +haut : boolean 
    +bas : boolean 
    +attaque : boolean 
    +Commande()
    +Commande(Commande)
}
class Controleur {
    +Controleur()
    +getCommande() : Commande 
    +keyPressed(KeyEvent) : void 
    +keyReleased(KeyEvent) : void 
    +keyTyped(KeyEvent) : void
}
class PanelDessin {
    -imageSuivante : BufferedImage 
    -imageEnCours : BufferedImage
    -width : int
    -height : int
    +PanelDessin(int, int, DessinJeu)
    +dessinerJeu() : void
    +paint(Graphics) : void
}
class InterfaceGraphique {
    +InterfaceGraphique(DessinJeu, int, int)
    +getControleur() : Controleur 
    +dessiner() : void
}
class MoteurGraphique {
    +MoteurGraphique(Jeu, DessinJeu)
    +lancerJeu(int, int) : void
}
class Main {
    +Main()
    +main(String[]) : void
}
interface DessinJeu {
    +dessiner(BufferedImage) : void
}

MoteurGraphique --> "0..1" InterfaceGraphique : -gui 
MoteurGraphique --> "0..1" DessinJeu : -dessin
MoteurGraphique --> "0..1" Jeu : -jeu

InterfaceGraphique --> "0..1" Controleur : -controleur
InterfaceGraphique --> "0..1" PanelDessin : -panel

Controleur --> "0..1" Commande : -commandeARetourner
Controleur --> "0..1" Commande : -commandeEnCours
InterfaceGraphique --> "0..1" Controleur : -controleur 
PannelDessin --> "0..1" DessinJeu : -dessin 
DessinGame ..|> DessinJeu
Jeu <|.. Donjon 
DessinGame --> "0..1" Donjon : -dj
DessinGame --> "0..1" Sprite : sprites


@enduml