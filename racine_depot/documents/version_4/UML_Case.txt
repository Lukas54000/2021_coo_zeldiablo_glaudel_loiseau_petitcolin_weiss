@startuml

class CaseLibre {
    +CaseLibre(int, int, Objet, Effet)
    +retirerObjet() : void
    +avoirObjet() : boolean
    +getObjet() : Objet
    +getEffet() : Effet
}
class CaseDepart {
    +CaseDepart(int, int)
}
class CaseSortie {
    +CaseSortie(int, int)
}
class CaseTrou {
    +CaseTrou(int, int)
    +remplirTrou() : void
}
class CaseGlace {
    +CaseGlace(int, int)
}
class CaseMur {
    +CaseMur(int, int)
}
abstract Case {
    -x : int 
    -y : int 
    #typeDeCase : int 
    {static} +CASE_ABSTRAITE : int 
    {static} +CASE_MUR : int 
    {static} +CASE_LIBRE : int 
    {static} +CASE_DEPART : int 
    {static} +CASE_SORTIE : int 
    {static} +CASE_TROU : int 
    {static} +CASE_GLACE : int
    +Case(int, int)
    +getX() : int 
    +getY() : int 
    +getTypeDeCase() : int 
    +setX(int) : void 
    +setY(int) : void
}

Case <|-- CaseMur 
CaseLibre <|-- CaseTrou
Case <|-- CaseLibre 

CaseLibre <|-- CaseGlace 
CaseLibre <|-- CaseSortie 
CaseLibre <|-- CaseDepart

@enduml