/***** PACKAGE *****/
package src;

/***** CLASSE ABSTRAITE MONSTRE */
public abstract class Monstre {

	/***** ATTRIBUTS *****/

	/**
	 * Attribut prive Case definissant la case ou se situe le monstre
	 */
	private Case cas;

	/**
	 * Attribut prive int, represente les points de vie du monstre
	 */
	private int pointsDeVie;

	/**
	 * Attribut prive int, definit la puissance d'attaque du monstre
	 */

	private int puissance;

	/**
	 * Attribut prive int type de monstre
	 */
	private int typeMonstre;

	/**
	 * constantes final entiere pour definir le type de monstre
	 */
	public final static int MONSTRE_IMMOBILE = 1;
	public final static int MONSTRE_ALEATOIRE = 2;
	public final static int MONSTRE_INTELLIGENT = 3;
	public final static int MONSTRE_TRES_INTELLIGENT = 4;

	/**
	 * constantes final entiere definissant les pv Max des Monstres
	 */
	public final static int PV_MONSTRE_IMMOBILE = 40;
	public final static int PV_MONSTRE_ALEATOIRE = 30;
	public final static int PV_MONSTRE_INTELLIGENT = 20;
	public final static int PV_MONSTRE_TRES_INTELLIGENT = 20;


	/***** CONSTRUCTEUR *****/

	public Monstre (Case c, int pv, int puiss) {
		this.cas = c;
		this.pointsDeVie = pv;
		this.puissance = puiss;
		this.typeMonstre = 0;
	}

	/***** METHODES *****/

	/**
	 * Methode qui modifie les pv du monstre en fonctions des degats du personnage.
	 * @param dgts degats de l'attaque.
	 */

	public void modifierPv(int dgts) {
		this.pointsDeVie -= dgts;
		if (this.pointsDeVie < 0) {
			this.pointsDeVie = 0;
		}
	}

	/**
	 * Methode qui teste si le monstre est mort
	 * @return boolean, true si mort, sinon false
	 */
	public boolean etreMort() {
		boolean res = false;
		if (this.pointsDeVie==0) {
			res = true;
		}
		return res;
	}

	/**
	 * 
	 * @param p Personnage pris en compte
	 * @return si le monstre est a portee du personnage ou non.
	 */

	public boolean persoAdjacent(Personnage p) {
		boolean res = false;
		Case c = p.getCase();
		if (Math.abs(c.getX() - this.cas.getX()) == 0 && Math.abs(c.getY() - this.cas.getY()) == 1
				|| Math.abs(c.getX() - this.cas.getX()) == 1 && Math.abs(c.getY() - this.cas.getY()) == 0) { //verifiation si il est dans le perimetre du monstre
			res = true;
		}
		return res;
	}

	/**
	 * Methode qui permet au monstre d'attaquer un personnage
	 * @param p Personnage pris en compte
	 */

	public void attaquer(Personnage p) {
        if (this.persoAdjacent(p)) {  //verification de si il y a un personnage dans le champs d'action du monstre
            p.modifierPv(-this.puissance);
        }
	}

	/**
	 * 
	 * @param dj Donjon 
	 * @return
	 */

	public abstract boolean seDeplacer(Donjon dj);

	/**
	 * Renvoit la case du monstre
	 * 
	 * @return l'attribut case
	 */
	public Case getCase() {
		return this.cas;
	}

	/**
	 * Definit la case du monstre
	 * 
	 * @param case
	 */
	public void setCase(Case c) {
		this.cas = c;
	}

	/**
	 * Renvoit les points de vie de l'entite
	 * 
	 * @return points de vie
	 */
	public int getVie() {
		return this.pointsDeVie;
	}

	/**
	 * permet de connaitre la puissance du monstre
	 * 
	 * @return l'attribut puissance
	 */

	public int getPuissance() {
		return this.puissance;
	}

	public int getTypeMonstre() {
		return this.typeMonstre;
	}

	public void setTypeMonstre(int t) {
		this.typeMonstre = t;
	}

}
