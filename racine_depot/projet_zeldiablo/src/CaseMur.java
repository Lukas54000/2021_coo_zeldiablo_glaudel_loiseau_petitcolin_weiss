/***** PACKAGE *****/
package src;

/***** CLASSE CASELIBRE (EXTEND CASE)*****/
public class CaseMur extends Case {

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur CaseMur avec 2 parametres
     * @param posX : int, position x de la case mur
     * @param posY : int, position y de la case mur
     */
    public CaseMur(int posX, int posY) {
        super(posX, posY);
        this.typeDeCase = Case.CASE_MUR;
    }

}