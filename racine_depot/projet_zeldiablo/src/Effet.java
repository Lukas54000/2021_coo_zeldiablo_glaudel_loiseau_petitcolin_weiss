/***** PACKAGE *****/
package src;

/***** INTERFACE OBJET *****/
public interface Effet {
    
    /***** METHODES *****/

    /**
     * execute le ou les effets de la case sur un personnage
     * @param p : personnage declanchant l'effet (peut etre null si c'est un monstre declanchant l'effet)
     * @param m : monstre declanchant l'effet (peut etre null si c'est un personnage declanchant l'effet)
     */
    public void declancherEffet(Personnage p, Monstre m);
}
