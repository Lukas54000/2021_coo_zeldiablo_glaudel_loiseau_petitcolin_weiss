/***** PACKAGE *****/
package src;

/***** CLASSE PIEGE (IMPLEMENTE EFFET) *****/
public class Piege implements Effet{

    /***** METHODES *****/

    /**
     * Methodes retirant des pvs de maniere plus ou moins differentes selon le declancheur 
     * @param p : Personnage declanchant le piege, null si un monstre declanche ce piege
     * @param m : Monstre declanchant le piege, null si un personnage declanche le piege
     */
    public void declancherEffet(Personnage p, Monstre m){
        if (p==null){
            m.modifierPv(5);
        } else {
            p.modifierPv(-8);
        }
    }
}
