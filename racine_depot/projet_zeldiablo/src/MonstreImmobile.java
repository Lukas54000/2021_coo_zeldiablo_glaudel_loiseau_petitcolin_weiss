/***** PACKAGE *****/
package src;

/***** CLASSE MONSTRE IMMOBILE */
public class MonstreImmobile extends Monstre {

   /***** CONSTRUCTEUR *****/

    public MonstreImmobile (Case c) {
        super(c, Monstre.PV_MONSTRE_IMMOBILE, 3);
        this.setTypeMonstre(Monstre.MONSTRE_IMMOBILE);
    }

    /***** METHODES *****/


    /**
     * seDeplacer a seulement le cas ou le boolean renvoit true si il y a un personnage dans son champs d'action.
     @param dj Donjon dans lequel se deplace le monstre
     */
    public boolean seDeplacer(Donjon dj) {
        boolean res = true;
        if (persoAdjacent(dj.getPersonnage())) {
            res = false;
        }
        return res;
    }
    
}
