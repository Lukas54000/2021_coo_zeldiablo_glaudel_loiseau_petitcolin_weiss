/***** PACKAGE *****/
package src;

/***** INTERFACE OBJET *****/
public interface Objet {

    /***** METHODES *****/

    /**
     * Execute la ou les fonctionnalite de l'Objet
     * @param p : Personnage, personnage a qui on attribue l'objet
     */
    public void executer(Personnage p);

    /**
     * Methode permettant d'identifier le type d'objet 
     * @return String : type d'objet
     */
    public String getTypeObjet();
}