/***** PACKAGE *****/
package src;

/***** IMPORTS *****/
import java.io.IOException;
import java.util.ArrayList;


import graphisme.*;

/***** CLASSE DONJON *****/
public class Donjon implements Jeu {

    /***** ATTRIBUT *****/

    /**
     * Attribut prive Personnage represantant le personnage joueur du donjon
     */
    private Personnage zelda;
    /**
     * Attribut prive Niveau representant le niveau en cours de partie
     */
    private Niveau niveau;
    /**
     * Attribut prive String[] contenant les fichiers textes representants les
     * niveaux du donjon
     */
    private String[] listeNiv = { "donnees/Niveau1.txt", "donnees/Niveau2.txt", "donnees/Niveau3.txt",
            "donnees/Niveau4.txt", "donnees/Niveau5.txt", "donnees/Niveau6.txt", "donnees/Niveau7.txt" };
    /**
     * Attribut prive int definissant le niveau actuelle jouer
     */
    private int nivEnCours = 0;
    /**
     * Attribut prive liste de Monstre, contenant tous les monstres du niveau en
     * cours
     */
    private ArrayList<Monstre> monstreDuNiveau;

    private boolean victoire;

    private int numTour, tempEntre2Attaques;

    public final static int INT_BETWEEN_2_ATTAQUES = 3;
    public final static int INT_VITESSE_MONSTRES = 8;
    public final static int NUM_DERNIER_NIVEAU = 6;

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur vide Donjon
     */
    public Donjon() {
        try {
            Niveau nv = new Niveau(listeNiv[this.nivEnCours]);
            this.niveau = nv;
            this.numTour = 0;
            this.tempEntre2Attaques = 0;
            this.zelda = new Personnage((CaseLibre)this.niveau.getCaseImportante(Case.CASE_DEPART));
            this.monstreDuNiveau = this.niveau.getMonstresInit();
            this.victoire=false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructeur special afin de realiser les tests
     * 
     * @param urlTest : String[], tableau de string contenant l'adresse de
     *                localisation en local des niveaux txt
     */
    public Donjon(String[] urlTest) {
        this.listeNiv = urlTest;
        try {
            Niveau nv = new Niveau(listeNiv[this.nivEnCours]);
            this.niveau = nv;
            this.zelda = new Personnage((CaseLibre)this.niveau.getCaseImportante(Case.CASE_DEPART));
            this.monstreDuNiveau = new ArrayList<Monstre>();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** METHODES */

    /**
     * Methode qui verifie que la case ou est le personnage soit la sortie ou
     * contient un objet, dans ce cas, en plus du mouvement precedent, une action
     * correspondante sera realisee
     */
    public void verifierNouvelleCase() {
        // faire attention au cas ou les monstres se deplacent
        Case c = this.zelda.getCase();
        if (c.getTypeDeCase() == Case.CASE_SORTIE && this.zelda.getAvoirCle()) {
            passerNiveauSuivant();
        }
        if (c.getTypeDeCase() == Case.CASE_LIBRE) {
            CaseLibre cl = (CaseLibre) c;

            if (cl.getObjet() != null) {
                this.zelda.getCase().getObjet().executer(this.zelda); // cas ou le personnage utilise l'objet
                this.zelda.getCase().retirerObjet();
            }
            if (cl.getEffet() != null) {
                this.zelda.getCase().getEffet().declancherEffet(this.zelda, null); // cas ou le personnage declanche
                                                                                   // l'effet
            }
        }
    }

    /**
     * Methode permettant de chercher un monstre dans le niveau
     * 
     * @param c qui est la case du monstre.
     * @return la case ou se trouve le monstre dans le donjon.
     */

    public Monstre chercherMonstre(Case c) {
        Monstre res = null;
        for (int i = 0; i < this.monstreDuNiveau.size(); i++) {
            if (this.monstreDuNiveau.get(i).getCase() == c) {
                res = this.monstreDuNiveau.get(i);
            }
        }
        return res;
    }

    /**
     * Methode qui charge le niveau suivant
     */
    public void passerNiveauSuivant() {
        this.nivEnCours++;
        try {
            this.niveau = new Niveau(listeNiv[nivEnCours]);
        } catch (IOException e) {
        }
        this.zelda.changerCle(); // assigne le parametre avoirCle a null pour ne pas avoir de cle au debut du
                                 // niveau.
        this.zelda.setCase((CaseLibre)this.niveau.getCaseImportante(Case.CASE_DEPART)); // fait debuter le personnage a la case
                                                                             // depart.
        this.monstreDuNiveau = this.niveau.getMonstresInit(); // initialise le monstres au niveau.
    }

    /**
     * Methode qui permet de savoir si une case contient une
     * entitee(monstre,personnage)
     * 
     * @param c Case, case qui conient une entitee
     * @return si la case possede une entitee ou non.
     */

    public boolean caseOccupee(Case c, boolean joueur) {
        boolean res = false;
        if (this.zelda.getCase() == c) {
            res = true;
        }
        for (int i = 0; i < this.monstreDuNiveau.size(); i++) {
            if (this.monstreDuNiveau.get(i).getCase() == c) {
                res = true;
            }
        }
        // Cas pour les monstres pour qu'ils ne traversent pas la pierre 
        // Le cas pour le joueur est traiter dans son deplacement avec le le deplacement de la pierre ou non
        if (!joueur) {
            if (this.niveau.getPierre()!=null) {
                if (this.niveau.getPierre().getCase()==c) {
                    res = true;
                }
            }
        }
        return res;
    }

    /***** GETTERS *****/

    public Personnage getPersonnage() {
        return this.zelda;
    }

    public Niveau getNiveau() {
        return this.niveau;
    }

    public int getNiveauEnCours() {
        return this.nivEnCours;
    }

    public ArrayList<Monstre> getMonstresDuNiveau() {
        return this.monstreDuNiveau;
    }

    public int getTempsEntre2Attaques() {
        return this.tempEntre2Attaques;
    }

    public boolean getVictoire(){
        return this.victoire;
    }

    /**
     * Methode d'ajout d'un monstre uniquement creer pour les test
     * 
     * @param m : Monstre, monstre a ajouter
     */
    public void addMonstre(Monstre m) {
        this.monstreDuNiveau.add(m);
    }

    /**
     * Methode qui gere l'action du joueur selon sa commande
     * 
     * @param commandeUser : Commande, commande du joueur
     */
    public void actionsJoueur(Commande commandeUser) {
        if (!this.zelda.getModeGlissage()) { //cas ou le personnage ce deplace d'une seule case car il ne glisse pas
            if (commandeUser.gauche) {
                this.zelda.seDeplacer(this,
                        this.niveau.getCase(this.zelda.getCase().getX() - 1, this.zelda.getCase().getY()));
                this.zelda.setDirection("Gauche");  //le personnage se deplace vers la gauche
            }
            if (commandeUser.droite) {
                this.zelda.seDeplacer(this,
                        this.niveau.getCase(this.zelda.getCase().getX() + 1, this.zelda.getCase().getY()));
                this.zelda.setDirection("Droite"); //le personnage se deplace vers la droite
            }
            if (commandeUser.bas) {
                this.zelda.seDeplacer(this,
                        this.niveau.getCase(this.zelda.getCase().getX(), this.zelda.getCase().getY() + 1));
                this.zelda.setDirection("Bas"); //le personnage se deplace vers le bas
            }
            if (commandeUser.haut) {
                this.zelda.seDeplacer(this,
                        this.niveau.getCase(this.zelda.getCase().getX(), this.zelda.getCase().getY() - 1));
                this.zelda.setDirection("Haut"); //le personnage se deplace vers le haut
            }
        } else { //cas ou le personnage glisse et peut se deplacer sur plusieures cases
            Case dir = null;
            switch (this.zelda.getDirection()) {
                case "Haut":
                dir = getNiveau().getCase(getPersonnage().getCase().getX(),
                            getPersonnage().getCase().getY() - 1);
                    break;
                case "Bas":
                dir = getNiveau().getCase(getPersonnage().getCase().getX(),
                            getPersonnage().getCase().getY() + 1);
                    break;
                case "Gauche":
                dir = getNiveau().getCase(getPersonnage().getCase().getX() - 1,
                            getPersonnage().getCase().getY());
                    break;
                case "Droite":
                dir = getNiveau().getCase(getPersonnage().getCase().getX() + 1,
                            getPersonnage().getCase().getY());
                break;
            } 
            this.zelda.seDeplacer(this, dir);
        }
        if (commandeUser.attaque && tempEntre2Attaques == 0) {
            tempEntre2Attaques = INT_BETWEEN_2_ATTAQUES;
            this.zelda.attaquer(this);
            this.verifierIntegriteMonstres();
        }
        
    }

    public void verifierIntegriteMonstres() {
        for (int i = 0; i < this.monstreDuNiveau.size(); i++) {
            if (this.monstreDuNiveau.get(i).etreMort()) {
                this.monstreDuNiveau.remove(this.monstreDuNiveau.get(i));
            }
        }
    }

    /**
     * Methode qui lancent les actions de tous les monstres du niveau
     */
    public void actionsMonstres() {
        for (int i = 0; i < this.monstreDuNiveau.size(); i++) {
            // dans les methodes se deplacaient, elle retourne false si le deplacement ne
            // peux pas avoir lieu, donc que le monstre doit attaquer
            boolean res = this.monstreDuNiveau.get(i).seDeplacer(this);
            if (!res) {
                this.monstreDuNiveau.get(i).attaquer(this.zelda);
            }
        }
    }

    /**
     * Methode evoluer qui permet de mettre a jour les donnees du jeu.
     */

    @Override
    public void evoluer(Commande commandeUser) {
        numTour++;
        if (tempEntre2Attaques != 0) {
            tempEntre2Attaques--;
        }
        actionsJoueur(commandeUser);
        // On definit que les monstres se deplacement tous les 10 rafraichissement de
        // l'image
        if (numTour >= INT_VITESSE_MONSTRES) {
            numTour = 0;
            actionsMonstres();
        }
    }

    /**
     * Methode etreFini qui permet de dire au jeu si c'est fini ou non.
     */

    @Override
    public boolean etreFini() {
        boolean res = false;
        if (!this.zelda.etreVivant()) { //si le personnage n'est plus vivant 
            res = true;
        } else if (this.getNiveauEnCours()==NUM_DERNIER_NIVEAU){
            this.victoire = true;
            res=true;
        }
        return res;
    }

}