package src;

public class Pierre  {
    private Case position;

    /**
     * Constructeur de Pierre avec 1 parametre
     * @param p la case dans laquelle se trouve la Pierre
     */
    public Pierre(Case p) {
        this.position = p;

    }
    

    /**
     * methode permettant de savoir si la Pierre change de case ou non
     * @param dj : donjon dans lequel se trouve la pierre
     * @param destination : future case qu'occuperait la pierre, elle va etre testee
     * @return boolean true si la pierre a bien change de case
     */
    public boolean changerCase(Donjon dj, Case destination) {
        boolean res = false;
        if (destination.getTypeDeCase() != Case.CASE_MUR) {  //si la case de destination est un mur
            if (destination.getTypeDeCase() == Case.CASE_TROU) {   //si la case de destination est bien un trou 
                CaseTrou c = (CaseTrou)dj.getNiveau().getTrou();  //on recupere la case
                c.remplirTrou(dj.getNiveau());  //on appelle la methode remplir trou qui permet a la pierre de transformer la case trou en case libre
                this.position = null;
                res = true;
            }
            else if (!dj.caseOccupee(destination, false)) {  // la case ne doit pas etre occupee
                    this.position = destination;
                    res = true;

            }
        } 
        return res;

    }

    public Case getCase(){
        return this.position;
    }

    
}
