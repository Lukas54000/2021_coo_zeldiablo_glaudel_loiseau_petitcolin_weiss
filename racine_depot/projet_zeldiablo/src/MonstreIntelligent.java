/***** PACKAGE *****/
package src;

/***** CLASSE MONSTREINTELLIGENT *****/
public class MonstreIntelligent extends Monstre {

    /***** CONSTRUCTEUR *****/
    public MonstreIntelligent(Case c) {
        super(c, Monstre.PV_MONSTRE_INTELLIGENT, 10);
        this.setTypeMonstre(Monstre.MONSTRE_INTELLIGENT);
    }

    /***** METHODES *****/

    @Override
    public boolean seDeplacer(Donjon dj) {
        boolean res = true;
        // On calcule la distance hauteur et largeur a vol d'oiseau
        int longueur = this.getCase().getX() - dj.getPersonnage().getCase().getX();
        int hauteur = this.getCase().getY() - dj.getPersonnage().getCase().getY();
        Case c;
        // Si la valeur absolue de longueur est plus grande ou egale a celle de hauteur
        if (Math.abs(longueur) >= Math.abs(hauteur)) {
            // On teste si pour se rapprocher du joueur le monstre doit aller a droite ou a gauche
            if (longueur <= 0) {
                c = dj.getNiveau().getCase(this.getCase().getX() + 1, this.getCase().getY());
            } else {
                c = dj.getNiveau().getCase(this.getCase().getX() - 1, this.getCase().getY());

            }
        } else { // Cas ou la valeur absolue de hauteur est plus grande que celle de longueur
            // On teste si pour se rapprocher du joueur, le monstre doit aller en haut ou en bas
            if (hauteur <= 0) {
                c = dj.getNiveau().getCase(this.getCase().getX() - 1, this.getCase().getY());
            } else {
                c = dj.getNiveau().getCase(this.getCase().getX(), this.getCase().getY() - 1);
            }
        }
        if (!dj.caseOccupee(c, false) && c.getTypeDeCase()!=Case.CASE_MUR) {
            this.setCase(c);
        }
        if (persoAdjacent(dj.getPersonnage())) {
            res = false;
        }
        return res;

    }

}
