/***** PACKAGE *****/
package src;

/***** CLASSE CASEGLACE (EXTEND CASE) *****/

public class CaseGlace extends CaseLibre {

     /***** CONSTRUCTEUR *****/

    /**
     * Constructeur CaseGlace avec 2 parametres
     * @param posX : int, position x de la case 
     * @param posY : int, position y de la case 
     */
    public CaseGlace(int posX, int posY) {
        super(posX, posY, null, null);
        this.typeDeCase = Case.CASE_GLACE;
    }

    
}
