/***** PACKAGE *****/
package src;

import java.util.*;
/***** CLASSE MONSTRE ALEATOIRE */

public class MonstreAleatoire extends Monstre {

    /***** CONSTRUCTEUR *****/

    public MonstreAleatoire(Case c) {
        super(c, Monstre.PV_MONSTRE_ALEATOIRE, 5);
        this.setTypeMonstre(Monstre.MONSTRE_ALEATOIRE);
    }

    /***** METHODES *****/

    /**
     * Le monstre peut se deplacer partout aleatoirement dans le donjon dans les
     * cases accessibles et non dans les murs.
     * 
     */
    public boolean seDeplacer(Donjon dj) {

        boolean res = true;
        if (persoAdjacent(dj.getPersonnage())) {
            res = false;
        } else {
            Case c = null;
            // determiner une case aleatoirement pour savoir si le monstre va vers le nord,
            // sud l'est ou l'ouest.
            Random rand = new Random();
            int r;
            r = rand.nextInt(3);
            switch (r) {
                case 0:
                    c = dj.getNiveau().getCase(this.getCase().getX() + 1, this.getCase().getY());
                    break;
                case 1:
                    c = dj.getNiveau().getCase(this.getCase().getX() - 1, this.getCase().getY());
                    break;
                case 2:
                    c = dj.getNiveau().getCase(this.getCase().getX() - 1, this.getCase().getY());
                    break;
                case 3:
                    c = dj.getNiveau().getCase(this.getCase().getX(), this.getCase().getY() - 1);
                    break;
            }
            // test si ce n'est pas un mur ou une case ayant une entitee
            // dessus(monstre,personnage)

            if (!dj.caseOccupee(c, false) && !(c.getTypeDeCase() == Case.CASE_MUR)) {
                this.setCase(c);
            }
        }

        return res;
    }
}
