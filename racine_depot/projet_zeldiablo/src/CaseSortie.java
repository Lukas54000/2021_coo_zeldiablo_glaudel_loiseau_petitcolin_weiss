/***** PACKAGE *****/
package src;

/***** CLASSE CASELIBRE (EXTEND CASE)*****/
public class CaseSortie extends CaseLibre {

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur CaseSortie avec 2 parametres
     * @param posX int: position x de la case sortie
     * @param posY int: position y de la case sortie
     */
    public CaseSortie(int posX, int posY) {
        super(posX, posY, null, null);
        this.typeDeCase = Case.CASE_SORTIE;
    }

}
