/***** PACKAGE *****/
package src;

/***** CLASSE CASE *****/
public abstract class Case {

    /***** ATTRIBUTS *****/

    /**
     * Attribut prive entier x et y contenant la position de la case dans le niveau
     */
    private int x, y;

    /**
     * Attribut protege entier qui definit le type de la case selon les constantes definies
     */
    protected int typeDeCase;

    /**
     * Constantes entieres permettant de facilement definir une case 
     */
    public final static int CASE_ABSTRAITE = 0;
    public final static int CASE_MUR = 1;
    public final static int CASE_LIBRE = 2;
    public final static int CASE_DEPART = 3;
    public final static int CASE_SORTIE = 4;
    public final static int CASE_TROU = 5;
    public final static int CASE_GLACE = 6;

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur Case avec 2 parametres
     * @param posX : int, position X de la case dans la grille
     * @param posY : int, position Y de la case dans la grille
     */
    public Case(int posX, int posY) {
        this.x = posX;
        this.y = posY;
        this.typeDeCase = CASE_ABSTRAITE;
    }

    /***** GETTERS *****/

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getTypeDeCase() {
        return this.typeDeCase;
    }

    /***** SETTERS *****/

    /**
     * methode modifiant le parametre x de la case utilise uniquement pour les tests
     * @param valX : nouvelle valeur de x
     */
    public void setX(int valX) {
        if (valX >= 0) {
            this.x = valX;
        }
    }

    /**
     * methode modifiant le parametre y de la case utilise uniquement pour les tests
     * @param valY : nouvelle valeur de y
     */
    public void setY(int valY) {
        if (valY >= 0) {
            this.y = valY;
        }
    }

}