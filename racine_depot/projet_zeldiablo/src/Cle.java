/***** PACKAGE *****/
package src;

/***** CLASSE CLE (IMPLEMENTE OBJET) *****/
public class Cle implements Objet {

    /**
     * Methode offrant une Cle au Personnage Cela lui permet d'atteindre le prochain
     * niveau du Donjon
     * @param p : Personnage, celui a qui on attribue l'objet Cle
     */
    public void executer(Personnage p) {
        p.changerCle();
    }

    public String getTypeObjet(){
        return "Cle";
    }
}