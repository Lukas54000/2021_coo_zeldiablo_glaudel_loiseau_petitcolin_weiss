/***** PACKAGE *****/
package src;

import java.util.*;

/***** CLASSE MONSTREINTELLIGENT *****/
public class MonstreTresIntelligent extends Monstre {

    private Case direction;

    /***** CONSTRUCTEUR *****/
    public MonstreTresIntelligent(Case c) {
        super(c, Monstre.PV_MONSTRE_TRES_INTELLIGENT, 20);
        this.setTypeMonstre(Monstre.MONSTRE_TRES_INTELLIGENT);
    }

    /***** METHODES *****/

    /**
     * Methode seDeplacer qui permet au monstre de se deplacer dans le donjon.
     *@param dj Donjon dans lequel le monstre se deplace
     */
    @Override
    public boolean seDeplacer(Donjon dj) {

        // on initialise les variables
        boolean res = false;
        // tableau de Case du niveau
        Case[][] tab = dj.getNiveau().getTabCase();
        // tableau d'entier contenant les nombres de pas
        int[][] tabAlgo = new int[10][10];
        Case caseTestee = dj.getPersonnage().getCase();
    
        // on cree une arrayListe de case contenant toutes les cases qu'on va tester une a une
        ArrayList<Case> listeCases = new ArrayList<Case>();
        
        // on initalise la case du personnage a 1
        tabAlgo[dj.getPersonnage().getCase().getX()][dj.getPersonnage().getCase().getY()] = 1;
        // on ajoute la case du personnage dans l'arrayListe
        listeCases.add(dj.getPersonnage().getCase());
    
        // tableau d'entier pour les 4 cases adjacentes
        int posX[] = {1,0,0,-1};
        int posY[] = {0,1,-1,0};
        // vaiables pour la case teste
        int x, y;
    
        // tant que la case du monstre vaut 0 dans tabAlgo
        while (tabAlgo[this.getCase().getX()][this.getCase().getY()]==0) {
            // on recupere la case a teste
            caseTestee = listeCases.get(0);
            // on scanne les 4 cases adjacentes
            for (int i=0; i<4; i++) {
                x = caseTestee.getX()+posX[i];
                y = caseTestee.getY()+posY[i];
                // si la case testee n'est pas un mur et vaut 0 dans tabAlgo alors on scanne ses 4 cases adjacentes, on recupere la plus grande valeur des cases de tabAlgo et on fait +1
                if (tab[x][y].getTypeDeCase()!=Case.CASE_MUR && tabAlgo[x][y]==0) {
                    int tmp = 0;
                    for (int j=0; j<4; j++) {
                        if (tabAlgo[x+posX[j]][y+posY[j]]>tmp) {
                            tmp = tabAlgo[x+posX[j]][y+posY[j]];
                        }
                    }
                    // on assigne une valeur dans tablAgo
                    tabAlgo[x][y] = tmp+1;
                    // on l'ajoute dans la liste des cases a testee
                    listeCases.add(tab[x][y]);
                }
            }
            // une fois les 4 cases adjacentes testes de la case test, on la supprime de la liste
            listeCases.remove(0);
        }
    

        Case caseRes = null;
        int tmp = 0;
        // une fois la case monstre dans tabAlgo ne vaut plus 0, un chemin existe donc entre perso et ce monstre
        // on scanne les 4 cases adjacentes du monstre a la recherche de la case tabAlgo valant -1 compare a celle du monstre dans tabAlgo
        for (int j=0; j<4; j++) {
            int valeur = tabAlgo[this.getCase().getX()+posX[j]][this.getCase().getY()+posY[j]];
            if (valeur>tmp) {
                tmp=valeur;
                caseRes = tab[this.getCase().getX()+posX[j]][this.getCase().getY()+posY[j]];
            }
        }
        // une fois la case ou le monstre doit se diriger intelligemment, on verifie que cette case est disponible donc qu'il n'y ait pas de monstre sur cette dite case
        if (!dj.caseOccupee(caseRes, false)) {
            this.setCase(caseRes); // on doit verifier que personne n'est dessus mais c'est deja sur d'etre une case Libre
            res=true;
        }
    
        return res;
    
    }


}
