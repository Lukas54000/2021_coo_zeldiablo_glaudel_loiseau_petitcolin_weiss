/***** PACKAGE *****/
package src;

/***** CLASSE POTION (IMPLEMENTE OBJET) *****/
public class Potion implements Objet {
    
    /***** METHODES *****/

    /**
     * Methode rendant des points de vie au personnage
     * Le nombre de pvs rendu est de 10
     * @param p : Personnage a qui on attribue la Potion
     */
    public void executer(Personnage p) {
        p.modifierPv(30);
    }
    
    public String getTypeObjet(){
        return "Potion";
    }
}