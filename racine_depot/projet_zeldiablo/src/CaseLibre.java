/***** PACKAGE *****/
package src;

/***** CLASSE CASELIBRE (EXTEND CASE) *****/
public class CaseLibre extends Case {

    /***** ATTRIBUTS *****/

    /**
     * Attribut prive Objet stockant l'objet ou non contenu sur la case
     */
    private Objet obj;
    /**
     * Attribut prive Effet stockant l'effet de la case s'il y en a un
     */
    private Effet effet;

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur CaseLibre (donc pas un mur) avec 4 parametres
     * @param posX : int, position x de la case
     * @param posY : int, position y de la case
     * @param o : Objet, objet qui est sur la case sinon null
     * @param e : Effet, effet que possede la case sinon null
     */
    public CaseLibre(int posX, int posY, Objet o, Effet e) {
        super(posX, posY);
        this.typeDeCase = Case.CASE_LIBRE;
        this.obj = o;
        this.effet = e;
    }

    /***** METHODES *****/

    /**
     * Methode pour retirer un objet de la case s'il y en a un
     */
    public void retirerObjet() {
        if (this.obj != null) {
            this.obj = null;
        }
    }

    /**
     * Methode qui retourne boolean indiquant si la case contient ou non un objet
     * 
     * @return boolean, true si la case a un objet sinon false
     */
    public boolean avoirObjet() {
        boolean res = false;
        if (this.obj != null) {  //si l'objet n'est pas null alors avoirObjet est vrai
            res = true;
        }
        return res;
    }

    /***** GETTERS *****/

    public Objet getObjet() {
        return this.obj;
    }

    public Effet getEffet() {
        return this.effet;
    }

}