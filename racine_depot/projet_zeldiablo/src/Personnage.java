/***** PACKAGE *****/
package src;

/***** CLASSE PERSONNAGE *****/
public class Personnage {

    /***** ATTRIBUTS *****/

    /**
     * Attribut prive Case qui contient la case ou se situe le personnage
     */
    private CaseLibre cas;
    /**
     * Attribut prive int representant les pv du personnage
     */
    private int pv;
    /**
     * Attribut privee boolean indiquant si le personnage a la clef du niveau
     */
    private boolean avoirCle;
    /**
     * Attribut private String indiquant la direction que prend le personnage
     */
    private String direction;
    /**
     * Attribut private int representant la force des coups du personnage
     */
    private int force;
    /**
     * Attribut final static indiquant les pv du personnage
     */
    public final static int PV_MAX_PERSO = 100;

    private boolean modeGlissade;

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur Personnage avec un parametre
     * 
     * @param c : Case, case ou se situe le personnage dans le niveau du donjon
     */
    public Personnage(CaseLibre c) {
        this.cas = c;
        this.pv = PV_MAX_PERSO;
        this.force = 10;
        this.avoirCle = false;
        this.direction = "Bas";
        this.modeGlissade = false;
    }

    /***** METHODES *****/

    /**
     * Methode qui teste si la case entrer en parametre est acessible pour le joueur
     * 
     * @param c : Case, case ou souhaite aller le joueur
     */
    public void seDeplacer(Donjon dj, Case c) {
        int dx = this.cas.getX() - c.getX();
        int dy = this.cas.getY() - c.getY();

        boolean lvl6 = (dj.getNiveau().getPierre() != null); // on verifiie si le niveau en cours n'est pas le niveau de
                                                             // sokoban

        if (Math.abs(dx) == 0 && Math.abs(dy) == 1 || Math.abs(dx) == 1 && Math.abs(dy) == 0) {
            if (!dj.caseOccupee(c, true)) {

                if (c.getTypeDeCase() != Case.CASE_MUR) {  // si la case est un mur 
                    if (lvl6) {  //si nous sommes au niveau 6
                        if (dj.getNiveau().getPierre().getCase() == c) {  // si la case est une pierre
                            if (dj.getNiveau().getPierre().changerCase(dj, getCaseDirection(dj, c))) {  
                                this.cas = (CaseLibre) c;
                            }
                        } else {
                            if (c.getTypeDeCase() != Case.CASE_TROU) //si la case est un trou 
                                this.cas = (CaseLibre) c;
                        }
                    } else {
                        this.cas = (CaseLibre) c;
                        if (cas.getTypeDeCase() == Case.CASE_GLACE) {  //si la case est une case glace
                            modeGlissade = true;  //le mode glissade est active c'est a dire que le personnage se deplace en glissant sur les cases glaces.
                        } else {
                            modeGlissade = false;
                        }

                    }
                } else {
                    modeGlissade = false;
                }
            } else {
                modeGlissade = false;
            }
        }
        dj.verifierNouvelleCase();
    }

    /**
     * Methode qui teste les pv du personnage et indique si le personnage est vivant
     * 
     * @return boolean, false si les pv sont a 0 sinon true
     */
    public boolean etreVivant() {
        boolean vivant = true;
        if (this.pv <= 0) {
            vivant = false;
        }
        return vivant;
    }

    /**
     * Methode qui modifie les pv du personnage selon le parametre int donnee
     * 
     * @param degats : int, valeur qui influence les pv du personnage
     */
    public void modifierPv(int degats) {
        this.pv += degats;
        if (this.pv > 100) {
            this.pv = 100;
        }
        if (this.pv <= 0) {
            this.pv = 0;
        }

    }

    /**
     * Methode qui fait attaquer le personnage selon la direction qu'il prend
     * 
     * @param antecedente case precedente qu'occupait le personnage
     * @param j           donjon dans lequel se trouve le prersonnage
     */
    public void attaquer(Donjon d) {
        /*
         * strategie : Pour commencer, on va determiner la direction que prend le
         * personnage pour que le donjon puisse tester si il existe une cible a cette
         * attaque et le faire perdre des pvs
         */

        // on defini une case qui sera la direction de l'attaque du personnage
        Case directionAttaque = getCaseDirection(d, this.cas);

        // on regarde s'il existe un monstre a la case direction et on l'attaque si
        // c'est le cas
        if (d.chercherMonstre(directionAttaque) != null) {
            d.chercherMonstre(directionAttaque).modifierPv(force);
        }
    }

    /**
     * methode permettant de connaitre la prochaine case selon la direction
     * @param d : donjon dans lequel se trouve la case
     * @param position : case qui va servir de referent pour determiner l'une des cases adjacentes qui va etre retournee selon la direction
     * @return la case qui suit cette direction
     */
    public Case getCaseDirection(Donjon d, Case position) {
        Case direct = null;
        switch (this.direction) {
            case "Haut":
                direct = d.getNiveau().getCase(position.getX(), position.getY() - 1);  //permet de connaitre la direction haut
                break;
            case "Bas":
                direct = d.getNiveau().getCase(position.getX(), position.getY() + 1); //permet de connaitre la direction bas
                break;
            case "Gauche":
                direct = d.getNiveau().getCase(position.getX() - 1, position.getY()); //permet de connaitre la direction geuche
                break;
            case "Droite":
                direct = d.getNiveau().getCase(position.getX() + 1, position.getY()); //permet de connaitre la direction droite
                break;
        }
        return direct;
    }

    /**
     * Methode qui change de true a false et inversement la possession de la cle du
     * personnage
     */
    public void changerCle() {
        if (this.avoirCle == false) {
            this.avoirCle = true;
        } else {
            this.avoirCle = false;
        }
    }

    /***** GETTERS *****/

    public CaseLibre getCase() {
        return this.cas;
    }

    public void setCase(CaseLibre c) {
        this.cas = c;
    }


    public int getPv() {
        return this.pv;
    }

    public boolean getAvoirCle() {
        return this.avoirCle;
    }

    public int getForce() {
        return this.force;
    }

    public String getDirection() {
        return this.direction;
    }

    public boolean getModeGlissage() {
        return this.modeGlissade;
    }

    /***** SETTERS *****/

    /**
     * methode modifiant le parametre s.
     * 
     * @param s nouvelle valeur de direction
     */

    public void setDirection(String s) {
        this.direction = s;
    }
}
