/***** PACKAGE *****/
package src;

/***** IMPORTS *****/
import java.io.*;
import java.util.ArrayList;

/***** CLASSE NIVEAU *****/
public class Niveau {

    /***** ATTRIBUTS *****/

    /**
     * Attribut prive tableau de case represenant le niveau du donjon
     */
    private Case[][] tabCase;
    /**
     * Attribut prive Case qui contient la case de depart et d'arrivee du donjon
     */
    private ArrayList<CaseLibre> listCases;
    /**
     * Attribut prive liste des monstres initialiser
     */
    private ArrayList<Monstre> listeMonstreInit;
    /**
     * Attribut prive Pierre (present que le niveau 6)
     */
    private Pierre pierre;
    private CaseTrou caseT;

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur Niveau avec 1 parametre
     * 
     * @param urlFichier : String, chemin ou se trouve le fichier texte a extraire
     *                   le niveau
     * @throws IOException : Exception a cause de la lecture d'un fichier texte
     */
    public Niveau(String urlFichier) throws IOException {
        this.tabCase = new Case[10][10];
        this.listeMonstreInit = new ArrayList<Monstre>();
        this.listCases = new ArrayList<CaseLibre>();
        FileReader txt = new FileReader(urlFichier);
        int caractere = -1;
        Case c = null;
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < 10; i++) {
                caractere = txt.read();
                while (caractere == 13 || caractere == 10) {
                    caractere = txt.read();
                }
                if (caractere == '#') { // cas pour un mur
                    c = new CaseMur(i, j);
                }
                if (caractere == ' ' || caractere == 'G') { // cas pour une case vide
                    c = new CaseLibre(i, j, null, null);
                }
                if (caractere == 'E') { // cas pour la sortie
                    c = new CaseSortie(i, j);
                    listCases.add((CaseLibre) c);
                }
                if (caractere == 'D') { // cas pour la case depart
                    c = new CaseDepart(i, j);
                    listCases.add((CaseLibre) c);
                }
                if (caractere == 'P') { // cas pour la potion
                    c = new CaseLibre(i, j, new Potion(), null);
                    listCases.add((CaseLibre) c);
                }
                if (caractere == 'C') { // cas pour la clef
                    c = new CaseLibre(i, j, new Cle(), null);
                    listCases.add((CaseLibre) c);
                }
                
                if (caractere == 'M') { // cas pour le monstre immobile
                    c = new CaseLibre(i, j, null, null);
                    listeMonstreInit.add(new MonstreImmobile(c));
                }
                if (caractere == 'N') { // cas pour le monstre intelligent
                    c = new CaseLibre(i, j, null, null);
                    listeMonstreInit.add(new MonstreIntelligent(c));
                }
                if (caractere == 'O') { // cas pour le monstre tres intelligent
                    c = new CaseLibre(i, j, null, null);
                    listeMonstreInit.add(new MonstreTresIntelligent(c));
                }
                if (caractere == 'L') { // cas pour le monstre aleatoire
                    c = new CaseLibre(i, j, null, null);
                    listeMonstreInit.add(new MonstreAleatoire(c));
                }
                if (caractere == 'G') { // cas pour la case glace
                    c = new CaseGlace(i, j);
                }
                if (caractere == 'T') { // cas pour une case libre avec un piege
                    c = new CaseLibre(i, j, null, new Piege());
                }
                if (caractere == 'W') { // cas pour une case trou
                    c = new CaseTrou(i, j);
                    this.caseT = (CaseTrou)c;

                }
                if (caractere == 'X') {  //cas pour la pierre 
                    c = new CaseLibre(i, j, null, null);
                    this.pierre = new Pierre(c);
                }

                // faire le cas du piege
                this.tabCase[i][j] = c;
            }
        }
        txt.close();
    }

    /***** METHODES *****/

    public ArrayList<Monstre> getMonstresInit() {
        return this.listeMonstreInit;
    }

    /***** GETTERS *****/

    public Case getCase(int x, int y) {
        return this.tabCase[x][y];
    }

    public Case[][] getTabCase(){
        return this.tabCase;
    }

    /**
     * methode qui retourne le type de case recherche selon les constantes entieres final, recherche parmis la liste des case importantes
     * @param typeCase : int, valeur du type de case rechercher dans le niveau
     * @return Case: la case recherchee ssi elle existe sinon null
     */
    public Case getCaseImportante(int typeCase) {
        Case c = null;
        for (int i = 0; i < this.listCases.size(); i++) {  
            if (listCases.get(i).getTypeDeCase() == typeCase)
                c = listCases.get(i);

        }
        return c;
    }

    /**
     * methode permettant de savoir quelle case libre possede l'objet mis en parametre 
     * @param : objetRecherche : objet recherche
     * @return : la CaseLibre en question
     */
    public CaseLibre getCaseObjet(String objetRecherche) {
        CaseLibre c = null;
        for (int i = 0; i < this.listCases.size(); i++) {
            if (listCases.get(i).getObjet() != null) {
                if (listCases.get(i).getObjet().getTypeObjet().equals(objetRecherche))
                    c = listCases.get(i);
            }
        }
        return c;
    }

    public Pierre getPierre() {
        return this.pierre;
    }

    public void pierreTombe() {
        this.pierre= null;
        this.caseT = null;
    }

    public Case getTrou() {
        return this.caseT;
    }

    /***** TOSTRING *****/

    public String toString() {
        String res = "";
        Case c = tabCase[0][0];
        CaseLibre cl = null;
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < 10; i++) {
                c = this.tabCase[i][j];
                if (c.getTypeDeCase() == Case.CASE_MUR) { // cas pour un mur
                    res += "#";
                } else if (c.getTypeDeCase() == Case.CASE_SORTIE) {  // cas pour la case sortie 
                    res += "S";
                } else if (c.getTypeDeCase() == Case.CASE_DEPART) {  // cas pour la case depart
                    res += "D";
                } else if (((CaseLibre) c).getObjet() != null) {  
                    if (((CaseLibre) c).getObjet().getTypeObjet().equals("Potion")) { // si l'objet est une potion
                        res += "P";
                    } else if (((CaseLibre) c).getObjet().getTypeObjet().equals("Cle")) { // si l'objet est une cle
                        res += "C";
                    }
                } else {
                    res += " ";
                }
            }
            res += "\n";
        }
        return res;
    }

}
