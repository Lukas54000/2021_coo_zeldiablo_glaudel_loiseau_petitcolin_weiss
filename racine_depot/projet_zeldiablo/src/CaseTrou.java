/***** PACKAGE *****/
package src;

/***** CLASSE CASETROU (EXTEND CASE) *****/
public class CaseTrou extends CaseLibre {

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur CaseTrou avec 2 parametres
     * 
     * @param posX : int, position x de la case du trou
     * @param posY : int, position y de la case du trou
     */
    public CaseTrou(int posX, int posY) {
        super(posX, posY, null, null);
        this.typeDeCase = Case.CASE_TROU;
    }

    /***** METHODES *****/

    public void remplirTrou(Niveau nv) {
        this.typeDeCase = Case.CASE_LIBRE; //le type de case est une case libre.
        nv.pierreTombe();
    }
}
