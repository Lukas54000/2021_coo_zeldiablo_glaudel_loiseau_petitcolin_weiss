/***** PACKAGE *****/
package src;

/***** CLASSE CASELIBRE (EXTEND CASELIBRE) *****/
public class CaseDepart extends CaseLibre {

    /***** CONSTRUCTEUR *****/

    /**
     * Constructeur CaseDepart avec 2 parametres
     * @param posX : int, position x de la case depart
     * @param posY : int, position y de la case depart
     */
    public CaseDepart(int posX, int posY) {
        super(posX, posY, null, null);
        this.typeDeCase = CASE_DEPART;
    }

}