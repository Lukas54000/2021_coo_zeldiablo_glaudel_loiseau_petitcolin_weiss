/***** PACKAGE *****/
package test;

/***** IMPORTS *****/
import static org.junit.Assert.*;
import org.junit.*;
import src.*;

/***** TEST CLASSE DONJON *****/
public class DonjonTest {

    private Donjon dj;

    @Before
    public void init() {
        String[] url = {
                "C:/Users/weiss/Desktop/2021_COO_Zeldiablo_Glaudel_Loiseau_Petitcolin_Weiss/racine_depot/donnees/Niveau1.txt" };
        dj = new Donjon(url);
    }

    @Test
    public void testDonjon() {
        System.out.println(dj.getNiveau());
        assertNotEquals("le donjon doit avoir un niveau en cours", null, dj.getNiveau());
        assertNotEquals("Le joueur alias Zelda ne doit pas etre null", null, dj.getPersonnage());
        assertEquals("joueur a x=1", 1, dj.getPersonnage().getCase().getX());
        assertEquals("joueur a y=5", 5, dj.getPersonnage().getCase().getY());
    }

}
