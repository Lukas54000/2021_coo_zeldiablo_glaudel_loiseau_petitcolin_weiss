/***** PACKAGE *****/
package test;

/***** IMPORTS *****/
import static org.junit.Assert.*;
import org.junit.*;
import src.*;

/***** TEST CLASSE CASE *****/
public class CaseTest {

    @Test
    public void testCase_OK() {
        CaseLibre c = new CaseLibre(5, 5, null, null);
        CaseMur m = new CaseMur(4, 7);
        assertEquals("Position x de la case a 5", 5, c.getX());
        assertEquals("Position y de la case a 5", 5, c.getY());
        assertEquals("Position x de la case a 4", 4, m.getX());
        assertEquals("Position y de la case a 7", 7, m.getY());
    }

    @Test
    public void testSetX_OK() {
        CaseLibre c = new CaseLibre(5, 5, null, null);
        c.setX(2);
        assertEquals("position x doit etre a 2", 2, c.getX());
        assertEquals("position y doit reste a 5", 5, c.getY());
    }

    @Test
    public void testSetX_Negatif() {
        CaseLibre c = new CaseLibre(5, 5, null, null);
        c.setX(-2);
        assertEquals("position x doit reste a 5", 5, c.getX());
        assertEquals("position y doit reste a 5", 5, c.getY());
    }

    @Test
    public void testSetY_OK() {
        CaseLibre c = new CaseLibre(5, 5, null, null);
        c.setY(2);
        assertEquals("position x doit reste a 5", 5, c.getX());
        assertEquals("position y doit etre a 5", 2, c.getY());
    }

    @Test
    public void testSetY_Negatif() {
        CaseLibre c = new CaseLibre(5, 5, null, null);
        c.setY(-2);
        assertEquals("position x doit reste a 5", 5, c.getX());
        assertEquals("position y doit reste a 5", 5, c.getY());
    }

    @Test 
    public void testAucunObjet(){
        CaseLibre c = new CaseLibre (5, 5, null, null);
        assertEquals("La case ne devrait pas avoir d'objet", false, c.avoirObjet());
    }

    
    @Test 
    public void testObjet(){
        Cle cl = new Cle(); 
        CaseLibre c = new CaseLibre (5, 5, cl, null);
        assertEquals("La case devrait avoir un objet", true, c.avoirObjet());
    }

    @Test 
    public void testRetirerObjet(){
        Cle cl = new Cle(); 
        CaseLibre c = new CaseLibre (5, 5, cl, null);
        c.retirerObjet();
        assertEquals("La case ne devrait pas avoir d'objet", false, c.avoirObjet());
    }

    @Test 
    public void testRetirerAucunObjet(){
        CaseLibre c = new CaseLibre (5, 5, null, null);
        c.retirerObjet();
        assertEquals("La case ne devrait pas avoir d'objet", false, c.avoirObjet());
    }
}