/***** PACKAGE *****/
package test;


/***** IMPORTS *****/
import static org.junit.Assert.*;
import org.junit.*;
import src.*;


/***** TEST CLASSE MONSTRE *****/
public class MonstreTest {
    

    @Test
    public void testMonstreImmobile_OK(){
        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});
        MonstreImmobile m = new MonstreImmobile(d.getNiveau().getCase(5,5));
        m.seDeplacer(d);

        assertEquals("Le monstre doit rester a cette case (X)", 5, m.getCase().getX());
        assertEquals("Le monstre doit rester a cette case (Y)", 5, m.getCase().getY());
    }

    /*
    @Test
    public void testMonstreIntelligent_PasPersonnage(){
        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});
        
        MonstreIntelligent m = new MonstreIntelligent(d.getNiveau().getCase(5, 5));

        m.seDeplacer(d);

        assertEquals("Le monstre doit rester a cette case (X)", 5, m.getCase().getX());
        assertEquals("Le monstre doit rester a cette case (Y)", 5, m.getCase().getY());    
    }*/

    @Test
    public void testMonstreIntelligent_PasMurEntrePersonnage(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreIntelligent m = new MonstreIntelligent(d.getNiveau().getCase(8, 4));
        d.getPersonnage().setCase((CaseLibre)d.getNiveau().getCase(5, 4));

        m.seDeplacer(d);

        assertEquals("Le Monstre doit etre en face du personnage (X)", 7, m.getCase().getX());
        assertEquals("Le Monstre doit etre en face du personnage (Y)", 4, m.getCase().getY());
    }

    @Test
    public void testMonstreIntelligent_MurEntrePersonnageGauche(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreIntelligent m = new MonstreIntelligent(d.getNiveau().getCase(6, 6));
        Personnage p = new Personnage((CaseLibre)d.getNiveau().getCase(4, 6));

        m.seDeplacer(d);

        assertEquals("Le Monstre ne doit pas etre en face du personnage (X)", 6, m.getCase().getX());
        assertEquals("Le Monstre  ne doit pas etre en face du personnage (Y)", 6, m.getCase().getY());

    }

    @Test
    public void testMonstreIntelligent_MurEntrePersonnageDroite(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreIntelligent m = new MonstreIntelligent(d.getNiveau().getCase(4, 6));
        Personnage p = new Personnage((CaseLibre)d.getNiveau().getCase(6, 6));

        m.seDeplacer(d);
        
        assertEquals("Le Monstre ne doit pas etre en face du personnage (X)", 4, m.getCase().getX());
        assertEquals("Le Monstre  ne doit pas etre en face du personnage (Y)", 6, m.getCase().getY());
    }

    @Test
    public void testMonstreIntelligent_PasDeplacementVerticalPasMur(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreIntelligent m = new MonstreIntelligent(d.getNiveau().getCase(4, 6));
        Personnage p = new Personnage((CaseLibre)d.getNiveau().getCase(3, 4));

        m.seDeplacer(d);

        assertEquals("Le Monstre ne doit pas avoir fait de deplacement diagonal (X)", 4, m.getCase().getX());
        assertEquals("Le Monstre ne doit pas avoir fait de deplacement diagonal (Y)", 4, m.getCase().getY());
    }

    @Test
    public void testMonstreIntelligent_PasDeplacementVerticaleMur(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreIntelligent m = new MonstreIntelligent(d.getNiveau().getCase(6, 6));
        Personnage p = new Personnage((CaseLibre)d.getNiveau().getCase(4, 4));

        m.seDeplacer(d);

        assertEquals("Le Monstre ne doit normalement pas se deplacer (X)", 4, m.getCase().getX());
        assertEquals("Le Monstre ne doit normalement pas se depalcer (Y)", 4, m.getCase().getY());
    }

    @Test
    public void testMonstreIntelligent_PasDeplacementVerticaleMurV2(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreIntelligent m = new MonstreIntelligent(d.getNiveau().getCase(4, 4));
        Personnage p = new Personnage((CaseLibre)d.getNiveau().getCase(6, 6));
        
        m.seDeplacer(d);

        assertNotEquals("Le Monstre doit s'etre deplacer (X)", 4, m.getCase().getX());
        assertNotEquals("Le Monstre doit s'etre deplacer (Y)", 4, m.getCase().getY());
    }


    /** Debut des tests de MonstreTresIntelligent */

    @Test
    public void testMonstreTresInteligent(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreTresIntelligent m = new MonstreTresIntelligent(d.getNiveau().getCase(8, 4));
        Personnage p = new Personnage((CaseLibre)d.getNiveau().getCase(1 , 5));

        m.seDeplacer(d);

        assertEquals("Le Monstre doit etre a cote du Personnage (X)", 1, m.getCase().getX());
        assertEquals("Le Monstre doit etre a cote du Personnage (Y)", 6, m.getCase().getY());
    }

    @Test
    public void testMonstreTresInteligent_TrajetLong(){

        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});

        MonstreTresIntelligent m = new MonstreTresIntelligent(d.getNiveau().getCase(1, 8));
        Personnage p = new Personnage((CaseLibre)d.getNiveau().getCase(8, 1));

        m.seDeplacer(d);

        assertEquals("Le Monstre doit etre a cote du Personnage (X)", 8, m.getCase().getX());
        assertEquals("Le Monstre doit etre a cote du Personnage (Y)", 2, m.getCase().getY());
    }


}
