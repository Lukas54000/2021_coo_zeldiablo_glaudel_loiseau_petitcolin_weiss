/***** PACKAGE *****/
package test;

/***** IMPORTS *****/
import static org.junit.Assert.*;
import org.junit.*;
import src.*;

/***** TEST CLASSE PERSONNAGE *****/
public class PersonnageTest {

    @Test
    public void testPersonnage() {
        CaseLibre c = new CaseLibre(5, 5, null, null);
        Personnage perso = new Personnage(c);
        assertEquals("x de la case a 5", 5, perso.getCase().getX());
        assertEquals("y de la case a 5", 5, perso.getCase().getX());
        assertEquals("case accessible", Case.CASE_LIBRE, c.getTypeDeCase());
        assertEquals("erreur : pas bons pv", 50, perso.getPv());
        assertEquals("erreur : ne peut pas avoir de cle", false, perso.getAvoirCle());
    }

    /**
     * test etreVivant
     */
    @Test
    public void testEtreVivant() {
        Personnage perso = new Personnage(null);
        assertEquals("personnage devrait etre vivant", true, perso.etreVivant());
    }

    /**
     * test modifierPv
     */
    @Test
    public void testModifierPv() {
        Personnage perso = new Personnage(null);
        assertEquals("erreur : pv pas a 50", 50, perso.getPv());
        perso.modifierPv(-1);
        assertEquals("perte de pv, perso doit etre a 49", 49, perso.getPv());

    }

    /**
     * test changerCle
     */
    @Test
    public void testChangerCle() {
        Personnage perso = new Personnage(null);
        assertEquals("erreur : le personnage ne doit pas avoir une cle", false, perso.getAvoirCle());
        perso.changerCle();
        assertEquals("erreur : le personnage doit avoir une cle", true, perso.getAvoirCle());
        perso.changerCle();
        assertEquals("erreur : le personnage ne doit plus avoir de cle", false, perso.getAvoirCle());
    }

    /**
    @Test
    public void testChangerCase_OK() {
        Donjon dj = new Donjon();
        seDeplacer(dj, );
        assertEquals("Le joueur etre toujours etre a x=1", 1, dj.getPersonnage().getCase().getX());
        assertEquals("Le joueur doit etre a y=6", 6, dj.getPersonnage().getCase().getY());
    }

    @Test
    public void testChangerCase_CaseX2() {
        dj.deplacerHero(dj.getNiveau().getCase(5, 7));
        assertEquals("Le joueur etre toujours etre a x=1", 1, dj.getPersonnage().getCase().getX());
        assertEquals("Le joueur doit toujours etre a y=5", 5, dj.getPersonnage().getCase().getY());
    }

    @Test
    public void testChangerCase_CaseDiagonale() {
        dj.deplacerHero(dj.getNiveau().getCase(2, 6));
        assertEquals("Le joueur etre toujours etre a x=1", 1, dj.getPersonnage().getCase().getX());
        assertEquals("Le joueur doit toujours etre a y=5", 5, dj.getPersonnage().getCase().getY());
    }

    @Test
    public void testChangerCase_CaseInacessible() {
        seDeplacer(dj);
        assertEquals("Le joueur etre toujours etre a x=1", 1, dj.getPersonnage().getCase().getX());
        assertEquals("Le joueur doit toujours etre a y=5", 5, dj.getPersonnage().getCase().getY());
    }
    */

    @Test
    public void testAttaque_Ok(){
        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});
        MonstreImmobile m = new MonstreImmobile(d.getNiveau().getCase(1, 6));
        d.addMonstre(m);
        d.getPersonnage().setDirection("Bas");
        d.getPersonnage().attaquer(d);
        assertEquals("Le monstre doit avoir subit une attaque", 10, m.getVie());
    }

    @Test
    public void testAttaque_MauvaiseDirection(){
        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});
        MonstreImmobile m = new MonstreImmobile(d.getNiveau().getCase(1, 6));
        d.addMonstre(m);
        d.getPersonnage().setDirection("Droite");
        d.getPersonnage().attaquer(d);
        assertEquals("Le monstre ne doit pas avoir subit d'attaque car le personnage n'est pas dirige vers le monstre", 20, m.getVie());
    }

    @Test
    public void testAttaque_HorsPortee(){
        Donjon d = new Donjon(new String[]{"C:/Users/weiss/Desktop/projet_zeldiablo/donnees/Niveau1.txt"});
        MonstreImmobile m = new MonstreImmobile(d.getNiveau().getCase(7, 4));
        d.addMonstre(m);
        d.getPersonnage().setDirection("Droite");
        d.getPersonnage().attaquer(d);
        assertEquals("Le monstre ne doit pas avoir subit d'attaque car il est trop eloigne du personnage", 20, m.getVie());
    }
}