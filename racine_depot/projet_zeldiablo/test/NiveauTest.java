/***** PACKAGE *****/
package test;

/***** IMPORTS *****/
import java.io.IOException;
import src.Niveau;

/***** TEST CLASSE NIVEAU *****/
public class NiveauTest {

    public static void main(String[] args) {
        Niveau niv;
        try {
            niv = new Niveau("donnees/Niveau1.txt");
            System.out.println(niv);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
