package graphisme;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import src.*;

public class DessinGame implements DessinJeu {

    /**
     * Donjon du jeu
     */
    private Donjon dj;
    /**
     * Objet contennant toutes les images graphiques du jeu
     */
    private Sprite sprites;

    /**
     * attribut prive entier qui retient les pv du personnage afin de savoir s'il
     * perd des pv ou s'il en gagne
     */
    private int pvPerso;
    /**
     * Attribut entier definissant la temps d'affichage des filtre vert et rouge
     * transparent signifiant qu'il perd ou gagne des pvs
     */
    private int timeAttaque, timeBlessure;
    /**
     * boolean permettant de savoir si le personnage gagne des pv (true) ou en perd
     * (false)
     */
    private boolean vieEnPlus = false;
    /**
     * attribut entier qui stocke la position x et y de l'attaque du joueur
     * (permettant d'afficher la cible verte de l'attaque)
     */
    private int posX, posY;

    /**
     * appelle constructeur parent
     * 
     * @param j le jeutest a afficher
     */
    public DessinGame(Donjon d) {
        this.dj = d;
        this.sprites = new Sprite();
        this.timeAttaque = 0;
        this.timeBlessure = 0;
        this.pvPerso = dj.getPersonnage().getPv();
    }

    /**
     * dessiner un objet consiste a dessiner sur l'image suivante methode redefinie
     * de Afficheur
     */
    private void dessinerObjet(BufferedImage im) {

        // on fait les tests de chaque occurence

        // cas ou les pv du personnage changent
        if (pvPerso != dj.getPersonnage().getPv()) {
            // on definit le temps d'affichage de l'ecran couleur
            timeBlessure = 4;
            // on definit si c'est une perte ou un gain de pv
            if (pvPerso > dj.getPersonnage().getPv()) {
                this.vieEnPlus = false;
            } else {
                this.vieEnPlus = true;
            }
            // on actualise la variable qui stocke les pv du joueur
            this.pvPerso = dj.getPersonnage().getPv();
        }

        // cas ou le joueur attaque
        if (dj.getTempsEntre2Attaques() == Donjon.INT_BETWEEN_2_ATTAQUES) {
            // on definit le temps d'affichage de la cible verte representant l'attaque du
            // joueur
            timeAttaque = 4;
            // on recupere la position du joueur et on definit la case ou il attaque (du a
            // l'attaque unidirectionnelle)
            String regard = dj.getPersonnage().getDirection();
            posX = dj.getPersonnage().getCase().getX();
            posY = dj.getPersonnage().getCase().getY();
            if (regard.equals("Haut"))
                posY--;
            if (regard.equals("Bas"))
                posY++;
            if (regard.equals("Droite"))
                posX++;
            if (regard.equals("Gauche"))
                posX--;
        }

        // on definit la taille x et y de la zone d'affichage du niveau
        int tailleNiveau = 600;
        // On cree un crayon g Graphics2D
        Graphics2D g = (Graphics2D) im.getGraphics();
        // On calcule la taille d'une case au sein de la fenetre
        int tailleCase = tailleNiveau / 10;
        // On definit a partir de quel pixel sur X le coin haut gauche du niveau sera
        int coinX = 150;
        // On definit la taille de la barre de vie de tous le monde
        int tailleBarreVie = tailleCase / 5 * 4;

        // On dessine le fond en pierre en background
        g.drawImage(sprites.murPierre, 0, 0, null);
        g.drawImage(sprites.murPierre, 400, 0, null);

        // On dessine maintenant le dessin du niveau qui est en cours
        g.drawImage(sprites.background[this.dj.getNiveauEnCours()], coinX, 0, tailleNiveau, tailleNiveau, null);

        if (dj.getNiveauEnCours() != Donjon.NUM_DERNIER_NIVEAU) { // Tous les niveaux sauf le dernier qui est la map de
                                                                  // fin
            if (!dj.getPersonnage().getAvoirCle()) {
                // On dessine l'escalier ferme au dessus de l'escalier ouvert ssi le personnage
                // n'a pas la cle
                // Condition egalement selon sa position car une fois en bas a gauche le niveau
                // suivant, ce sera en haut a droite
                if (dj.getNiveauEnCours() % 2 == 1) {
                    g.drawImage(sprites.escalierFermeBas, coinX, 0, tailleNiveau, tailleNiveau, null);
                } else {
                    g.drawImage(sprites.escalierFermeHaut, coinX, 0, tailleNiveau, tailleNiveau, null);
                }
            }
        }

        // On dessine sur la carte la cle ssi elle n'a pas etait encore prise
        if (this.dj.getNiveau().getCaseObjet("Cle") != null) {
            if (this.dj.getNiveau().getCaseObjet("Cle").getObjet().getTypeObjet().equals("Cle")) {
                g.drawImage(sprites.clef, coinX + dj.getNiveau().getCaseObjet("Cle").getX() * tailleCase,
                        dj.getNiveau().getCaseObjet("Cle").getY() * tailleCase, tailleCase, tailleCase, null);
            }
        }

        // On dessine sur la carte la potion ssi elle existe et si elle n'a pas etait
        // prise
        if (this.dj.getNiveau().getCaseObjet("Potion") != null) {
            if (this.dj.getNiveau().getCaseObjet("Potion").getObjet().getTypeObjet().equals("Potion")) {
                g.drawImage(sprites.potion,
                        coinX + dj.getNiveau().getCaseObjet("Potion").getX() * tailleCase + tailleCase / 3,
                        dj.getNiveau().getCaseObjet("Potion").getY() * tailleCase + tailleCase / 3, tailleCase / 4 * 2,
                        tailleCase / 4 * 2, null);
            }
        }

        // On dessine maitenant tous les monstres
        ArrayList<Monstre> list = this.dj.getMonstresDuNiveau();
        for (int i = 0; i < list.size(); i++) {
            // on recupere le monstre i de la liste des monstres du niveau
            Monstre m = list.get(i);
            // on definit une variable pour avoir les pv max du monstre en question et pour
            // avoir son sprite
            int pvMax = 20;
            BufferedImage image = null;
            // 4 if afin de recuperer l'image du monstre en question ainsi que ses pvMax
            if (m.getTypeMonstre() == Monstre.MONSTRE_IMMOBILE) {
                image = sprites.monstreImmobile;
                pvMax = Monstre.PV_MONSTRE_IMMOBILE;
            }
            if (m.getTypeMonstre() == Monstre.MONSTRE_ALEATOIRE) {
                image = sprites.monstreAleatoire;
                pvMax = Monstre.PV_MONSTRE_ALEATOIRE;
            }
            if (m.getTypeMonstre() == Monstre.MONSTRE_INTELLIGENT) {
                image = sprites.monstreIntelligent;
                pvMax = Monstre.PV_MONSTRE_INTELLIGENT;
            }
            if (m.getTypeMonstre() == Monstre.MONSTRE_TRES_INTELLIGENT) {
                image = sprites.monstreTresIntelligent;
                pvMax = Monstre.PV_MONSTRE_TRES_INTELLIGENT;
            }
            // On dessine le monstre
            g.drawImage(image, coinX + m.getCase().getX() * tailleCase,
                    m.getCase().getY() * tailleCase, tailleCase, tailleCase, null);
            // On dessine une barre pleine rouge pour les pv et on dessine au dessus celle
            // en verte selon les pv du monstre
            g.setColor(Color.RED);
            g.fillRect(coinX + m.getCase().getX() * tailleCase + tailleCase / 5,
                    m.getCase().getY() * tailleCase, tailleBarreVie, 4);
            int tailleBarreVieActuelle = tailleBarreVie * list.get(i).getVie() / pvMax;
            g.setColor(Color.GREEN);
            g.fillRect(coinX + m.getCase().getX() * tailleCase + tailleCase / 5,
                    m.getCase().getY() * tailleCase, tailleBarreVieActuelle, 4);
        }

        // On dessine la pierre dans le niveau et le trou pour la pierre
        if (dj.getNiveau().getPierre() != null) {
            g.drawImage(sprites.pierre, dj.getNiveau().getPierre().getCase().getX() * tailleCase + coinX,
                    dj.getNiveau().getPierre().getCase().getY() * tailleCase, tailleCase, tailleCase, null);
            Case ct = dj.getNiveau().getTrou();
            g.drawImage(sprites.trou, ct.getX() * tailleCase + coinX, ct.getY() * tailleCase, tailleCase, tailleCase,
                    null);
        }

        // Maintenant, on dessine le personnage au sein du niveau
        g.drawImage(sprites.perso, coinX + dj.getPersonnage().getCase().getX() * tailleCase,
                dj.getPersonnage().getCase().getY() * tailleCase, tailleCase, tailleCase, null);

        // On dessine la barre de vie du joueur au dessus du personnage
        g.setColor(Color.RED);
        g.fillRect(coinX + dj.getPersonnage().getCase().getX() * tailleCase + tailleCase / 5,
                dj.getPersonnage().getCase().getY() * tailleCase, tailleBarreVie, 4);
        int tailleBarreVieActuelleJoueur = tailleBarreVie * dj.getPersonnage().getPv()
                / dj.getPersonnage().PV_MAX_PERSO;
        g.setColor(Color.GREEN);
        g.fillRect(coinX + dj.getPersonnage().getCase().getX() * tailleCase + tailleCase / 5,
                dj.getPersonnage().getCase().getY() * tailleCase, tailleBarreVieActuelleJoueur, 4);

        // On dessine la cible ssi une attaque vient d'avoir lieu et timeAttaque diminue
        // de 1 a chaque refraichissement
        if (timeAttaque != 0) {
            timeAttaque--;
            g.drawImage(sprites.cible, coinX + posX * tailleCase, posY * tailleCase, tailleCase, tailleCase, null);
        }

        // Un calque semi-transparent s'affiche, en rouge pour la perte de pv, en vert
        // pour le gain de pv
        // timeBlessure se desincremente de 1 a chaque rafraichissement
        if (timeBlessure != 0) {
            timeBlessure--;
            // Pour finir, qu'on soit blesser un filtre rouge semi transparent remplit
            // l'ecran
            if (vieEnPlus) {
                g.setColor(new Color(0, 225, 90, 50));
            } else {
                g.setColor(new Color(255, 0, 0, 50));
            }
            g.fillRect(0, 0, 1000, 800);
        }

        // si on est en fin de partie (defaite ou victoire), on affiche un bordereau
        // blanc avec l'image correspondant
        if (dj.etreFini()) { // Affichage Message de Fin de Partie quand le personnag est mort ou en cas de
                             // victoire
            g.setColor(new Color(255, 255, 255));
            g.fillRect(0, 200, 900, 200);
            if (dj.getVictoire()) {
                g.drawImage(sprites.victoire, 300, 200, 350, 200, null);
            } else {
                g.drawImage(sprites.defaite, 300, 200, 350, 200, null);
            }
        }
    }

    /**
     * methode dessiner redefinie de Afficheur retourne une image du jeu
     */
    public void dessiner(BufferedImage im) {
        this.dessinerObjet(im);
    }

}
