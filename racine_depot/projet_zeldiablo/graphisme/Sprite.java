package graphisme;

import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;

import src.*;

public class Sprite {
    
    /***** ATTRIBUTS *****/
    private String[] listeNiveau = {"donnees/niv1.png", "donnees/niv2.png", "donnees/niv3.png", "donnees/niv4.png", "donnees/niv5.png", "donnees/niv6.png", "donnees/niv7.png"};
    public BufferedImage[] background;
    public BufferedImage backgroundBois, murPierre;
    public BufferedImage escalierFermeHaut, escalierFermeBas;
    public BufferedImage perso;
    public BufferedImage clef, potion, cible;
    public BufferedImage monstreImmobile, monstreAleatoire, monstreIntelligent, monstreTresIntelligent;
    public BufferedImage pierre, trou;
    public BufferedImage victoire, defaite;

    /***** CONSTRUCTEURS *****/
    public Sprite() {
        try {
            
            this.murPierre = ImageIO.read(new File("donnees/murPierre.jpg"));
            this.background = new BufferedImage[listeNiveau.length];
            for (int i=0; i<listeNiveau.length; i++) {
                this.background[i] = ImageIO.read(new File(listeNiveau[i]));
            }
            this.perso = ImageIO.read(new File("donnees/perso.png"));
            this.clef = ImageIO.read(new File("donnees/clef.png"));
            this.escalierFermeHaut = ImageIO.read(new File("donnees/escalierHautDroite.png"));
            this.escalierFermeBas = ImageIO.read(new File("donnees/escalierBasGauche.png"));
            this.potion = ImageIO.read(new File("donnees/potion.png"));
            this.backgroundBois = ImageIO.read(new File("donnees/textureBois.jpg"));
            this.cible = ImageIO.read(new File("donnees/cible.png"));

            this.monstreAleatoire = ImageIO.read(new File("donnees/monstreAlea.png"));
            this.monstreImmobile = ImageIO.read(new File("donnees/monstreImmobile.png"));
            this.monstreIntelligent = ImageIO.read(new File("donnees/monstreIntelligent.png"));
            this.monstreTresIntelligent = ImageIO.read(new File("donnees/monstreTresIntelligent.png"));

            this.pierre = ImageIO.read(new File("donnees/roche.png"));
            this.trou = ImageIO.read(new File("donnees/trou.png"));

            this.victoire = ImageIO.read(new File("donnees/victoire.png"));
            this.defaite  = ImageIO.read(new File("donnees/defaite.jpg"));


        } catch (IOException e) {}
    }

}
