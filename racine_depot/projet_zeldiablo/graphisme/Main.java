package graphisme;

import src.Donjon;

public class Main {
    
    public static void main (String[] args) throws Exception {

        // On cree un Donjon et un DessinGame
        Donjon dj = new Donjon();
        DessinGame dg = new DessinGame(dj);

        // On cree un MoteurGraphique et on lance le jeu
        MoteurGraphique moteur = new MoteurGraphique(dj, dg);
        moteur.lancerJeu(900, 600);

    }

}
